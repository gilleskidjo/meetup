import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Meetups from '@/views/Meetup/Meetups.vue'
import CreateMeetup from '@/views/Meetup/CreateMeetup.vue'
import Profil from '@/views/User/Profil.vue'
import Signin from '@/views/User/Signin.vue'
import Signup from '@/views/User/Signup.vue'
import VueCarousel from 'vue-carousel'
import Meetup from '@/views/Meetup/Meetup.vue'

Vue.use(VueRouter)
Vue.use(VueCarousel)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/meetups',
    name: 'meetups',
    component: Meetups
  },
  {
    path: '/meetups/:id',
    name: 'Meetup',
    props: true,
    component: Meetup
  },
  {
    path: '/meetup',
    name: 'meetup',
    component: Meetup
  },
  {
    path: '/meetup/new',
    name: 'CreateMeetup',
    component: CreateMeetup
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup
  },
  {
    path: '/profil',
    name: 'Profil',
    component: Profil
  },
  {
    path: '/signin',
    name: 'Signin',
    component: Signin
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
