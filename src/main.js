import Vue from 'vue'
import App from './App.vue'
import router from './router'
import 'bulma/css/bulma.min.css'
import { store } from './store'
import * as firebase from 'firebase'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyD2w-ub57FneaC62h4V1M4SioI1oZqtdEc',
      authDomain: 'meetup-aa603.firebaseapp.com',
      databaseURL: 'https://meetup-aa603.firebaseio.com',
      projectId: 'meetup-aa603',
      storageBucket: 'meetup-aa603.appspot.com'
    })
    this.$store.dispatch('loadMeetups')
  }
}).$mount('#app')
